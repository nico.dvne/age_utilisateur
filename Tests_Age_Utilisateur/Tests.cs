﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Age_Utilisateur;

namespace Tests_Age_Utilisateur
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Test_Quel_Age_As_Tu()
        {
            Assert.AreEqual(true, Program.Quel_Age_As_Tu(2000));
            Assert.AreEqual(true, Program.Quel_Age_As_Tu(1900));
            Assert.AreEqual(false, Program.Quel_Age_As_Tu(2018));
        }
    }
}
